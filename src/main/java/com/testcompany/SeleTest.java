package com.testcompany;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

public class SeleTest {
    List<String> info;
    public void setInfo (List info) {
        this.info = info;
    }

    private WebDriver driver;
    private WebElement webElement;

    public static final String WEB_DRIVER_ID = "webdriver.chrome.driver";
    public static final String WEB_DRIVER_PATH = "chromedriver.exe";

    private String logIn_url;
    private String idNameId;
    private String idName;
    private String pwNameId;
    private String pwName;
    private String logInName;
    private String testUrl;
    private int quantity;
    private String makeText;
    private String titleIdText;
    private String textIdText;
    private String keyWordText;
    private String registerText;


    public SeleTest() {
        super();
        //System Property SetUp
        System.setProperty(WEB_DRIVER_ID, WEB_DRIVER_PATH);
        //Driver SetUp
        ChromeOptions options = new ChromeOptions();
        options.setCapability("ignoreProtectedModeSettings", true);
        driver = new ChromeDriver(options);
        //logIn_url = info.get(0);
    }

    public void crawl() {

        try {
            //get page (= 브라우저에서 url을 주소창에 넣은 후 request 한 것과 같다)
            logIn_url = info.get(0);
            driver.get(logIn_url);

            //iframe으로 구성된 곳은 해당 프레임으로 전환시킨다.
            idNameId = info.get(1);
            //driver.switchTo().frame(driver.findElement(By.id(idNameId)));

            //iframe 내부에서 id 필드 탐색
            webElement = driver.findElement(By.id(idNameId));
            idName =info.get(2);
            webElement.sendKeys(idName);

            pwNameId = info.get(3);
            webElement = driver.findElement(By.id(pwNameId));
            pwName =info.get(4);
            webElement.sendKeys(pwName);
            //webElement.submit();

            //iframe 내부에서 pw 필드 탐색
            //webElement = driver.findElement(By.id("inputPwd"));
            //String daum_pw ="your pw";
            //webElement.sendKeys(daum_pw);

            //로그인 버튼 클릭
           /* logInName = info.get(5);
            webElement = driver.findElement(By.className(logInName));
            StringTokenizer stk = new StringTokenizer(logInName);
            StringBuilder sb = new StringBuilder();

            while(stk.hasMoreTokens()) {
                sb.append(stk.nextToken());
                if (stk.hasMoreTokens())
                    sb.append(".");
            }

            webElement = driver.findElement(By.cssSelector(sb.toString()));
*/
            driver.findElement(By.xpath("//*[@id=\"login-form\"]/fieldset/div[8]/button")).click();

            quantity = Integer.parseInt(info.get(7));

            for (int i = 0 ; i<quantity ; i++ ) {

                testUrl = info.get(6);
                driver.get(testUrl);

                // frame switching
                driver.switchTo().frame("BlogMain");

                //makeText = info.get(9);
                //driver.switchTo().frame(driver.findElement(By.id(makeText)));
               // webElement = driver.findElement(By.id(makeText));
                //webElement.submit();

                titleIdText = info.get(10);
                webElement = driver.findElement(By.id(titleIdText));

                keyWordText = info.get(8);
                webElement.sendKeys(keyWordText);

                textIdText = info.get(11);
                webElement = driver.findElement(By.id(textIdText));
                webElement.sendKeys(keyWordText);

                registerText = info.get(12);
                webElement = driver.findElement(By.className(registerText));
                webElement.click();

            }


            Thread.sleep(20000);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            driver.close();
        }

    }

}
