package com.testcompany;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class TextFieldOne extends JFrame implements ActionListener {
    Button btnGo;
    TextField keyWordText;
    TextField urlText;
    TextField quantityText;
    TextField logInUrlText;
    TextField idNameIdText;
    TextField pwNameIdText;
    TextField idNameText;
    TextField pwNameText;
    TextField logInNameText;
    TextField makeText;
    TextField titleIdText;
    TextField textIdText;
    TextField regesterText;
    public TextFieldOne(String str)  {
        super(str);
        setLayout(new GridLayout(14,2) );
        Label logInUrl = new Label("로그인 페이지 URL");
        Label idNameId = new Label("ID 입력 영역");
        Label idName = new Label("ID 입력");
        Label pwNameId = new Label("pw 입력 영역");
        Label pwName = new Label("pw 입력");
        Label logInName = new Label("로그인 버튼");
        Label url = new Label("페이지 URL 입력");
        Label make = new Label("글쓰기 버튼");
        Label quantity = new Label("원하는 수량 입력");
        Label keyWord = new Label("키워드 입력");
        Label titleId = new Label("제목 입력 영역");
        Label textId = new Label("본문 내용 입력 영역");
        Label register = new Label("글 등록 버튼");
        urlText = new TextField("", 20);
        quantityText = new TextField("3",5);
        keyWordText = new TextField("하이 하이",30);
        logInUrlText = new TextField("https://accounts.kakao.com/login?continue=https%3A%2F%2Flogins.daum.net%2Faccounts%2Fksso.do%3Frescue%3Dtrue%26url%3Dhttp%253A%252F%252Fblog.daum.net%252Fsbux",20);
        idNameIdText = new TextField("id_email_2",5);
        pwNameIdText = new TextField("id_password_3",5);
        idNameText = new TextField("",10);
        pwNameText = new TextField("",10);
        logInNameText = new TextField("",5);
        titleIdText = new TextField("simpleWriter-title",5);
        textIdText = new TextField("simpleWriter-content",5);
        makeText = new TextField("smp_submit",5);
        regesterText = new TextField("smp_submit",5);
        btnGo = new Button("실행");
        btnGo.addActionListener(this);
        add(logInUrl);
        add(logInUrlText);
        add(idNameId);
        add(idNameIdText);
        add(idName);
        add(idNameText);
        add(pwNameId);
        add(pwNameIdText);
        add(pwName);
        add(pwNameText);
        add(logInName);
        add(logInNameText);
        add(url);
        add(urlText);
        add(make);
        add(makeText);
        add(quantity);
        add(quantityText);
        add(keyWord);
        add(keyWordText);
        add(titleId);
        add(titleIdText);
        add(textId);
        add(textIdText);
        add(register);
        add(regesterText);
        add(btnGo);
        setTitle("자동 글 생성");
        setSize(400,300);
       setVisible(true);
       super.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

    }


    @Override
    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if((Button)obj == btnGo) {
            ArrayList<String> getInfo = new ArrayList<>();
            getInfo.add(logInUrlText.getText());
            getInfo.add(idNameIdText.getText());
            getInfo.add(idNameText.getText());
            getInfo.add(pwNameIdText.getText());
            getInfo.add(pwNameText.getText());
            getInfo.add(logInNameText.getText());
            getInfo.add(urlText.getText());
            getInfo.add(quantityText.getText());
            getInfo.add(keyWordText.getText());
            getInfo.add(makeText.getText());
            getInfo.add(titleIdText.getText());
            getInfo.add(textIdText.getText());
            getInfo.add(regesterText.getText());

    getInfo.get(0);
            SeleTest st = new SeleTest();
            st.setInfo(getInfo);
            st.crawl();

            //for (String x : getInfo) {
            //    System.out.println(x);
            //}
        }
    }
}